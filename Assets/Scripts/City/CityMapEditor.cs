﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.City
{
    [CustomEditor(typeof(CityMap))]
    public class CityMapEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            if (GUILayout.Button("Check Graph"))
                (target as CityMap)?.CheckGraph();
        }
    }
}
