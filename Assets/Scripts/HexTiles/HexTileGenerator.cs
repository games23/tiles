using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Common;
using Assets.Scripts.Common.Graphs;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.HexTiles
{
    public class HexTileGenerator : MonoBehaviour
    {
        [SerializeField] Tile[] tilePrefabs;
        [SerializeField] Vector2 tileSize;
        [SerializeField] int mapWidth;
        [SerializeField] int mapHeight;
        [SerializeField] Transform container;
        [SerializeField] float oddRowOffset;
        [SerializeField] float rockChance;


        [Header("REFS")]
        [SerializeField] Movement movement;

        List<Tile> tileMap;
        TileMapHelper mapHelper;

        void Start()
        {
            tileMap = new List<Tile>();
            mapHelper = new TileMapHelper(tileMap, GetNeighbours);

            for (int x = 0; x < mapWidth; x++)
            {
                for (int y = 0; y < mapHeight; y++)
                {
                    var candidate = PickCandidate();
                    var tile = Instantiate(candidate, container);
                    if(y.IsOdd())
                        tile.Initialize(new Coordinate(x,y), tileSize, oddRowOffset, GoToTile, mapHelper);
                    else
                        tile.Initialize(new Coordinate(x, y), tileSize, 0, GoToTile, mapHelper);

                    tileMap.Add(tile);
                }
            }
            
            movement.Initialize(mapHelper);
        }

        Tile PickCandidate() => Random.value < rockChance ?
                tilePrefabs.Where(t => !t.IsPassable).PickOne() :
                tilePrefabs.Where(t => t.IsPassable).PickOne();

        void GoToTile(Coordinate to)
        {
            var from = movement.Coordinate;
            var path = GraphHelper.GetPathFromTo(mapHelper.GetTileIn(from), mapHelper.GetTileIn(to), tileMap);

            movement.FollowPath(path.Cast<Tile>());
        }

        IEnumerable<Coordinate> GetNeighbours(Coordinate center, Func<Coordinate, bool> canMoveTo) =>
            center.Y.IsOdd()
                ? new[] { center.GetN, center.GetE, center.GetS, center.GetSE, center.GetW, center.GetNE }.Where(canMoveTo)
                : new[] { center.GetN, center.GetE, center.GetS, center.GetSW, center.GetW, center.GetNW }.Where(canMoveTo);
    }
}