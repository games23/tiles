using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Common.Graphs;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Common
{
    public class Tile : MonoBehaviour, Node
    {
        [SerializeField] SpriteRenderer sprite;
        [SerializeField] Gradient tint;
        [SerializeField] bool isPassable;
        [SerializeField] Text coordDisplay;

        Action<Coordinate> onClick;
        IEnumerable<Node> neighbors;

        public Coordinate Coordinate { get; private set; }
        public bool IsPassable => isPassable;

        public void Initialize(Coordinate c, Vector2 tileSize, float xOffset, Action<Coordinate> onClick, TileMapHelper mapHelper)
        {
            this.onClick = onClick;
            this.Coordinate = c;
            transform.localPosition = new Vector3(c.X * tileSize.x + xOffset, c.Y * tileSize.y, 0);
            sprite.color = tint.Evaluate(Random.value);
            coordDisplay.text = Coordinate.ToString();

            neighbors = mapHelper.GetNeighbours(c).Select(c => mapHelper.GetTileIn(c));
        }

        public void OnMouseDown()
        {
            Debug.Log($"clicked on tile {Coordinate.ToString()}");
            onClick(Coordinate);
        }

        public IEnumerable<Node> Neighbors() => neighbors;

        public Vector3 PositionInSpace() => transform.position;
    }
}