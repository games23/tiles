﻿

using System;
using UnityEngine;

namespace Assets.Scripts.Common
{
    [Serializable]
    public struct Coordinate
    {
        [SerializeField] int x;
        [SerializeField] int y;
        public int X => x;
        public int Y => y;

        public Coordinate(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public override string ToString() => $"({x},{y})";
        public bool Equals(Coordinate other) => other.x == x && other.y == y;

        public Coordinate GetN => new Coordinate(X, Y + 1);
        public Coordinate GetNE => new Coordinate(X + 1, Y + 1);
        public Coordinate GetE => new Coordinate(X + 1, Y);
        public Coordinate GetSE => new Coordinate(X + 1, Y - 1);
        public Coordinate GetS => new Coordinate(X, Y - 1);
        public Coordinate GetSW => new Coordinate(X - 1, Y - 1);
        public Coordinate GetW => new Coordinate(X - 1, Y);
        public Coordinate GetNW => new Coordinate(X - 1, Y + 1);

        public Vector2 ToVector2() => 
            new Vector2(X, Y);
    }
}
