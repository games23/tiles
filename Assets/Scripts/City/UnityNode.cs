﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Common.Graphs;
using UnityEngine;

namespace Assets.Scripts.City
{
    public abstract class UnityNode : MonoBehaviour, Node
    {
        [SerializeField]
        protected List<UnityNode> neighbors;
        
        public IEnumerable<Node> Neighbors() => neighbors;

        public Vector3 PositionInSpace() =>
            transform.position;

        void OnDrawGizmos()
        {
            foreach (var n in neighbors.Where(n => n != null))
                Debug.DrawLine(transform.position, n.transform.position, Color.blue);
        }
    }
}
