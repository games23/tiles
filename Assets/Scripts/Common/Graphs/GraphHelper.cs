﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Common.Graphs
{
    public class GraphHelper
    {
        public static IEnumerable<Node> GetPathFromTo(Node from, Node to, IEnumerable<Node> graph)
        {
            var distances = new Dictionary<Node, float>();
            var previous = new Dictionary<Node, Node>();
            var visited = new List<Node>();

            distances.Add(from, DistanceFromTo(from, to));
            previous.Add(from, null);
            var current = from;

            while (PathNotFound(to, visited, current, graph))
            {
                visited.Add(current);

                foreach (var n in current.Neighbors())
                {
                    if (!previous.ContainsKey(n))
                        distances[n] = DistanceFromTo(from, n) + DistanceFromTo(current, n);

                    if (!previous.ContainsKey(n))
                        previous.Add(n, current);
                }

                var candidatesByDistance = distances
                    .OrderBy(d => d.Value)
                    .Select(d => d.Key)
                    .Except(visited);

                if (candidatesByDistance.Empty())
                    return new[] { from };

                current = candidatesByDistance.First();
            }

            return BuildPath(previous, from, to);
        }

        static float DistanceFromTo(Node from, Node to) =>
            Vector3.Distance(from.PositionInSpace(), to.PositionInSpace());

        static bool PathNotFound(Node to, List<Node> visited, Node current, IEnumerable<Node> graph) =>
            visited.Count < graph.Count() && current != to;

        static IEnumerable<Node> BuildPath(Dictionary<Node, Node> previous, Node from, Node to)
        {
            var path = new List<Node>();
            var node = to;
            while (previous[node] != null)
            {
                path.Add(node);
                node = previous[node];
            }

            path.Add(from);
            path.Reverse();

            return path;
        }
    }
}