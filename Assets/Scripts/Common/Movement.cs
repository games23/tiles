﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Common
{
    public class Movement : MonoBehaviour
    {
        [SerializeField] Coordinate startingPosition;
        [SerializeField] float moveSpeed;
        [SerializeField] float proximityThreshold;

        TileMapHelper mapHelper;
        Coordinate coordinate;

        public Coordinate Coordinate => coordinate;

        public void Initialize(TileMapHelper mapHelper)
        {
            this.mapHelper = mapHelper;
            MoveTo(mapHelper.GetTileIn(startingPosition));
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
                MoveUp();
            if (Input.GetKeyDown(KeyCode.DownArrow))
                MoveDown();
            if (Input.GetKeyDown(KeyCode.LeftArrow))
                MoveLeft();
            if (Input.GetKeyDown(KeyCode.RightArrow))
                MoveRight();
        }

        void MoveUp()
        {
            if (mapHelper.CanMoveTo(coordinate.GetN))
                MoveTo(mapHelper.GetTileIn(coordinate.GetN));
        }

        void MoveDown()
        {
            if (mapHelper.CanMoveTo(coordinate.GetS))
                MoveTo(mapHelper.GetTileIn(coordinate.GetS));
        }

        void MoveLeft()
        {
            if (mapHelper.CanMoveTo(coordinate.GetW))
                MoveTo(mapHelper.GetTileIn(coordinate.GetW));
        }

        void MoveRight()
        {
            if (mapHelper.CanMoveTo(coordinate.GetE))
                MoveTo(mapHelper.GetTileIn(coordinate.GetE));
        }

        void MoveTo(Tile target)
        {
            coordinate = target.Coordinate;
            transform.position = target.transform.position;
        }

        public void FollowPath(IEnumerable<Tile> path)
        {
            var trimmedPath = path.ToList();
            trimmedPath.Remove(path.First());
            StartCoroutine(MoveTo(path.First(), path));
        }

        IEnumerator DoPathNode(IEnumerable<Tile> path)
        {
            if (path.Any())
            {
                var trimmedPath = path.ToList();
                trimmedPath.Remove(path.First());
                yield return new WaitForSeconds(moveSpeed);
                MoveTo(path.First());
                StartCoroutine(DoPathNode(trimmedPath));
            }
        }

        IEnumerator MoveTo(Tile nextTile, IEnumerable<Tile> path)
        {
            var targetPosition = nextTile.transform.position;
            var direction = transform.InverseTransformPoint(targetPosition).normalized;

            while (Vector3.Distance(targetPosition, transform.position) > proximityThreshold)
            {
                transform.Translate(direction * moveSpeed * Time.fixedDeltaTime);
                yield return null;
            }

            coordinate = nextTile.Coordinate;
            transform.position = targetPosition;
            NextPathNode(path);
        }

        void NextPathNode(IEnumerable<Tile> path)
        {
            if (path.Any())
            {
                var trimmedPath = path.ToList();
                trimmedPath.Remove(path.First());
                StartCoroutine(MoveTo(path.First(), trimmedPath));
            }
        }
    }
}
