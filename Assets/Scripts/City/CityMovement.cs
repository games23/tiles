﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Common;
using Assets.Scripts.Common.Graphs;
using UnityEngine;

namespace Assets.Scripts.City
{
    public class CityMovement : MonoBehaviour
    {
        [SerializeField] float moveSpeed;
        [SerializeField] float proximityThreshold;


        public void FollowPath(IEnumerable<Node> path)
        {
            var trimmedPath = path.ToList();
            trimmedPath.Remove(path.First());
            StartCoroutine(MoveTo(path.First(), path));
        }

        IEnumerator MoveTo(Node next, IEnumerable<Node> path)
        {
            var targetPosition = next.PositionInSpace();
            var direction = transform.InverseTransformPoint(targetPosition).normalized;

            while (Vector3.Distance(targetPosition, transform.position) > proximityThreshold)
            {
                transform.Translate(direction * moveSpeed * Time.fixedDeltaTime);
                yield return null;
            }

            transform.position = targetPosition;
            NextPathNode(path);
        }

        void NextPathNode(IEnumerable<Node> path)
        {
            if (path.Any())
            {
                var trimmedPath = path.ToList();
                trimmedPath.Remove(path.First());
                StartCoroutine(MoveTo(path.First(), trimmedPath));
            }
        }
    }
}
