﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Scripts.Common
{
    public class TileMapHelper
    {
        List<Tile> map;
        Func<Coordinate, Func<Coordinate, bool>, IEnumerable<Coordinate>> getNeighbors;


        public TileMapHelper(List<Tile> map, Func<Coordinate, Func<Coordinate, bool>, IEnumerable<Coordinate>> getNeighbors)
        {
            this.getNeighbors = getNeighbors;
            this.map = map;
        }

        public bool CanMoveTo(Coordinate target) =>
            map.Any(t => t.Coordinate.Equals(target)) &&
            GetTileIn(target).IsPassable;


        public Tile GetTileIn(Coordinate target) => map.First(t => t.Coordinate.Equals(target));

        public IEnumerable<Coordinate> GetNeighbours(Coordinate center) => getNeighbors(center, CanMoveTo);
    }
}