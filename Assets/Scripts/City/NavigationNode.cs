﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Common.Graphs;
using UnityEngine;

namespace Assets.Scripts.City
{
    public class NavigationNode : UnityNode
    {
        public void CreateNeighbor()
        {
            var neighbor = Instantiate(this, transform.parent);
            neighbors.Add(neighbor);
            neighbor.ResetWith(this);
        }

        void ResetWith(NavigationNode other)
        {
            neighbors = new List<UnityNode>();
            neighbors.Add(other);
            transform.position = other.transform.position + Vector3.up;
            gameObject.name = (int.Parse(other.gameObject.name) + 1).ToString();
        }
    }
}
