﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Common.Graphs;
using UnityEngine;

namespace Assets.Scripts.City
{
    public class CityMap : MonoBehaviour
    {
        [SerializeField]
        CityMovement character;

        IEnumerable<Node> navigationMap;
        
        void Start()
        {
            navigationMap = GetComponentsInChildren<Node>();

            foreach (var cn in GetComponentsInChildren<ClickableNode>())
                cn.Initialize(OnClick);
        }

        void OnClick(ClickableNode to)
        {
            var from = GetClosestNodeToCharacter(character.transform.position);
            var path = GraphHelper.GetPathFromTo(from, to, navigationMap);
            character.FollowPath(path);
        }

        Node GetClosestNodeToCharacter(Vector3 character) =>
            navigationMap
                .Select(n => new { distance = Vector3.Distance(character, n.PositionInSpace()), node = n})
                .OrderBy(ncd => ncd.distance)
                .First().node;

        public void CheckGraph()
        {
            var graph = GetComponentsInChildren<UnityNode>();
            foreach (var node in graph)
            {
                if(node.Neighbors().Any(nei => !nei.Neighbors().Contains(node)))
                    throw new Exception($"node {node.gameObject.name} is not attached to some of its neighbors");
            }
        }
    }
}
