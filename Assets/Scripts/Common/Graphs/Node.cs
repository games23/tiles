﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Common.Graphs
{
    public interface Node
    {
        IEnumerable<Node> Neighbors();
        Vector3 PositionInSpace();
    }
}
