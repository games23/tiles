﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.City
{
    [CustomEditor(typeof(NavigationNode))]
    public class NavigationNodeEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            if (GUILayout.Button("Create Neighbor"))
                (target as NavigationNode)?.CreateNeighbor();
        }
    }
}
