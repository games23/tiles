﻿using System;

namespace Assets.Scripts.City
{
    public class ClickableNode : UnityNode
    {
        Action<ClickableNode> onClick;

        void OnMouseDown() => 
            onClick(this);

        public void Initialize(Action<ClickableNode> onClick) => 
            this.onClick = onClick;
    }
}
