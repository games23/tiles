using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Common;
using Assets.Scripts.Common.Graphs;
using UnityEngine;

namespace Assets.Scripts.SquareTiles
{
    public class SquareTilesGenerator : MonoBehaviour
    {
        [SerializeField] Tile[] tilePrefabs;
        [SerializeField] Vector2 tileSize;
        [SerializeField] int mapWidth;
        [SerializeField] int mapHeight;
        [SerializeField] Transform container;

        [Header("REFS")]
        [SerializeField] Movement movement;

        TileMapHelper mapHelper;
        List<Tile> tileMap;

        void Start()
        {
            tileMap = new List<Tile>();

            for (int x = 0; x < mapWidth; x++)
            {
                for (int y = 0; y < mapHeight; y++)
                {
                    var candidate = tilePrefabs.PickOne();
                    var tile = Instantiate(candidate, container);
                    tile.Initialize(new Coordinate(x,y), tileSize, 0, GoToTile, mapHelper);
                    tileMap.Add(tile);
                }
            }

            mapHelper= new TileMapHelper(tileMap, GetNeighbours);
            movement.Initialize(mapHelper);
        }

        void GoToTile(Coordinate to)
        {
            var from = movement.Coordinate;
            var path = GraphHelper.GetPathFromTo(mapHelper.GetTileIn(from), mapHelper.GetTileIn(to), tileMap);

            movement.FollowPath(path.Cast<Tile>());
        }

        IEnumerable<Coordinate> GetNeighbours(Coordinate center, Func<Coordinate, bool> canMoveTo) =>
            new[]
            {
                center.GetN, center.GetNE, center.GetE, center.GetSE, center.GetS, center.GetSW, center.GetW,
                center.GetNW
            }.Where(canMoveTo);
    }
}